package sbu.cs;

public class Blue extends Square {

    private String input1; //left
    private String input2; //above
    private String[] output = new String[2];

    public Blue(String input1 , String input2 ,int functionNum) {
        super(functionNum);
        this.input1 = input1;
        this.input2 = input2;
    }

    @Override
    public String[] getOutput() {
        switch (functionNum){
            case 1 :
                Black1 black1 = new Black1();
                output[0] = black1.func(input1);
                output[1] = black1.func(input2);
                break;

            case 2 :
                Black2 black2 = new Black2();
                output[0] = black2.func(input1);
                output[1] = black2.func(input2);
                break;

            case 3 :
                Black3 black3 = new Black3();
                output[0] = black3.func(input1);
                output[1] = black3.func(input2);
                break;

            case 4 :
                Black4 black4 = new Black4();
                output[0] = black4.func(input1);
                output[1] = black4.func(input2);
                break;

            case 5 :
                Black5 black5 = new Black5();
                output[0] = black5.func(input1);
                output[1] = black5.func(input2);
                break;
        }
        return output;
    }
}
