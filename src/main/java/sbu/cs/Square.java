package sbu.cs;

public abstract class Square {
    protected int functionNum;
    abstract public String[] getOutput();
    public Square(int functionNum){
        this.functionNum = functionNum;
    }
}
