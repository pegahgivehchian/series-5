package sbu.cs;

import java.util.ArrayList;
import java.util.List;

public class SortArray {

    /**
     * sort array arr with selection sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] selectionSort(int[] arr, int size) {
            List<Integer> list = new ArrayList();
            int[] answer = new int[size];

            for (int i = 0 ; i < size ; i ++){
                list.add(arr[i]);
            }

            for (int i = list.size()-1  ; i >= 0 ; i --){
                int minPlace = 0;
                int min = list.get(0);
                for (int j = i ; j >= 0 ; j --){
                    if (list.get(j) < min){
                        min = list.get(j);
                        minPlace= j;
                    }
                }
                list.remove(minPlace);
                answer[arr.length - i - 1] = min;
            }
            return answer;
    }

    /**
     * sort array arr with insertion sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size) {
        List<Integer> list = new ArrayList();
        int[] answer = new int[arr.length];

        for (int i = 0 ; i < arr.length ; i ++){
            list.add(arr[i]);
        }

        for (int i = 0 ; i < list.size() ; i ++){
            int place = 0 ;
            for (int j = 0 ; j < i ; j ++) {
                if (list.get(i) > list.get(j)) {
                    place++;
                }
            }
            list.add(place , list.get(i));
            list.remove(i+1);
            System.out.println(list);
        }
        for (int i = 0 ; i < list.size() ; i ++){
            answer[i] = list.get(i);
        }
        return answer;
    }


    /**
     * sort array arr with merge sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] mergeSort(int[] arr, int size) {
        if (size <= 1){
            return arr;
        }
        int[] first = new int [arr.length/2];
        int[] second = new int [arr.length - first.length];
        for (int i = 0 ; i < first.length ; i ++){
            first[i] = arr[i];
        }

        for (int i = first.length ; i < arr.length ; i ++){
            second[i- first.length] = arr[i];
        }

        first = mergeSort(first , first.length);
        second = mergeSort(second , second.length);
        arr = merge(first,second,arr);
        return arr;
    }

    public int[] merge (int[] first , int[] second , int[] a){
        int i = 0 , j = 0 , z = 0;
        while(i < first.length && j < second.length){
            if (first[i] > second[j] && j < second.length) {
                a[z] = second[j];
                j++;
            }
            else {
                a[z] = first[i];
                i++;
            }

            z++;
        }
        while ( i < first.length){
            a[z] = first[i];
            z ++;
            i ++;
        }
        while ( j < second.length){
            a[z] = second[j];
            z ++;
            j ++;
        }
        return a;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] arr, int value) {
        int num1= 0 , num2= arr.length-1 ;
        while (returnArea(num1,num2,value,arr)[0] != returnArea(num1,num2,value,arr)[1]){
            num1 = returnArea(num1,num2,value,arr)[0];
            num2 = returnArea(num1,num2,value,arr)[1];
        }
        if (value == arr[returnArea(num1,num2,value,arr)[0]])
            return returnArea(num1,num2,value,arr)[0];
        else
            return -1;
    }

    public int[] returnArea (int num1 , int num2 , int value , int[]arr ){

        int[] area;
        int middle = (num1 + num2) / 2 ; //belongs to left half
        if (value <= arr[middle]) {
            num2 = middle ;
        }
        else{
            num1 = middle + 1;
        }
        area = new int[]{num1, num2};
        return area;
    }
    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearchRecursive(int[] arr, int value) {
        int num1= 0 , num2= arr.length-1 ;
        return search(num1 , num2 , value , arr);
    }

    public int search (int num1 , int num2 , int value , int[]arr ){
        if (value == arr[num1]){
            return num1;
        }
        else if (value == arr[num2]){
            return num2;
        }
        else if (num2 - num1 == 0){
            return -1;
        }
        int middle = (num1+num2)/2;
        if (value > arr[middle]){
            return search(middle + 1 , num2 , value , arr);
        }
        else {
            return search(num1, middle , value , arr);
        }
    }
}
