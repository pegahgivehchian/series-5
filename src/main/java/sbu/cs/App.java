package sbu.cs;

public class App {

    /**
     * use this function for magical machine question.
     *
     * @param n     size of machine
     * @param arr   an array in size n * n
     * @param input the input string
     * @return the output string of machine
     */
        private int n;
        private int[][] functionNumArr;
        private   String input;
        private Square[][] colorArr;

        public String main(int n, int[][] arr, String input) {
            this.n = n;
            this.functionNumArr = arr;
            this.input = input;
            colorArr = new Square[n][n];
            colorArr[0][0] = new Green(input , functionNumArr[0][0]);
            for (int i = 0 ; i < n ; i ++){
                for (int j = 0 ; j < n ; j ++){

                    if (i==0 && j!=0 && j < n - 1 ) {
                        colorArr[i][j] = new Green(colorArr[i][j-1].getOutput()[0] , functionNumArr[i][j] );
                    }
                    else if(j==0 && i < n-1 && i != 0){
                        colorArr[i][j] = new Green(colorArr[i-1][j].getOutput()[0] , functionNumArr[i][j] );
                    }
                    else if (i==0 && j==n-1){
                        colorArr[i][j] = new Yellow(colorArr[i][j-1].getOutput()[0] , functionNumArr[i][j] );
                    }
                    else if (i==n-1 && j==0){
                        colorArr[i][j] = new Yellow(colorArr[i-1][j].getOutput()[0] , functionNumArr[i][j] );
                    }
                    else if ( i==1 && j>0 && j<n-1){
                        colorArr[i][j] = new Blue(colorArr[i][j-1].getOutput()[0], colorArr[i-1][j].getOutput()[0], functionNumArr[i][j] );
                    }
                    else if (i>1 && j>0 && j<n-1 && i<n-1){
                        colorArr[i][j] = new Blue(colorArr[i][j-1].getOutput()[0],colorArr[i-1][j].getOutput()[1] , functionNumArr[i][j] );

                    }
                    else if ((j==n-1 && i>0)){
                        colorArr[i][j] = new Pink(colorArr[i][j-1].getOutput()[0],colorArr[i-1][j].getOutput()[0] , functionNumArr[i][j] );
                    }
                    else if ((i==n-1 && j>0)){
                        colorArr[i][j] = new Pink(colorArr[i][j-1].getOutput()[0],colorArr[i-1][j].getOutput()[1] , functionNumArr[i][j] );
                    }

                }
            }
            return colorArr[n-1][n-1].getOutput()[0];
        }

    }


