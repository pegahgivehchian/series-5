package sbu.cs;

public class Green extends Square {

    private String input;
    private String[] output = new String[2];

    Green(String input, int functionNum){
        super(functionNum);
        this.input = input;
    }

    @Override
    public String[] getOutput() {
        switch (functionNum){
            case 1 :
                Black1 black1 = new Black1();
                output[0] = black1.func(input);
            break;

            case 2 :
                Black2 black2 = new Black2();
                output[0] = black2.func(input);
            break;

            case 3 :
                Black3 black3 = new Black3();
                output[0] = black3.func(input);
                break;

            case 4 :
                Black4 black4 = new Black4();
                output[0] = black4.func(input);
                break;

            case 5 :
                Black5 black5 = new Black5();
                output[0] = black5.func(input);
                break;
        }
        return output;
    }


}
