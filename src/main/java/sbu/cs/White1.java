package sbu.cs;

public class White1 implements WhiteFunction{


    @Override
    public String func(String arg1, String arg2) {
        StringBuilder sb = new StringBuilder("");
        if (arg1.length() > arg2.length()){
            sb.append(arg1);
            int j = 1 ;
            for (int i = 0 ; i < arg2.length() ; i ++){
                sb.insert( j , arg2.charAt(i));
                j +=2 ;
            }
        }
        else{
            sb.append(arg2);
            int j = 0 ;
            for (int i = 0 ; i < arg1.length() ; i ++){
                sb.insert( j , arg1.charAt(i));
                j +=2 ;
            }
        }
        return sb.toString();
    }
}
