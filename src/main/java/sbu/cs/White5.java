package sbu.cs;

public class White5 implements WhiteFunction{

    @Override
    public String func(String arg1, String arg2) {
        if (arg1.length() > arg2.length()){
            return mix(arg1,arg2);

        }
        else {
            return mix(arg2,arg1);
        }
    }

    private String mix(String str1 , String str2){
        StringBuilder sb = new StringBuilder(str1);
        for (int i = 0 ; i < str2.length() ; i ++){
            int num = (((int)str1.charAt(i) + (int) str2.charAt(i) - 97*2 ) % 26 )+ 97 ;
            char chr = (char) num;
            sb.setCharAt(i , chr);
        }
        return sb.toString();
    }


}
