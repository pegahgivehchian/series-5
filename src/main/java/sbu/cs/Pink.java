package sbu.cs;

public class Pink extends Square {
    private String input1 , input2;
    private String[] output = new String[2];

    public Pink(String input1 , String input2 ,int functionNum) {
        super(functionNum);
        this.input1 = input1;
        this.input2 = input2;
    }

    @Override
    public String[] getOutput() {
        switch (functionNum){
            case 1 :
                White1 white1 = new White1();
                output[0] = white1.func(input1 , input2);
                break;

            case 2 :
                White2 white2 = new White2();
                output[0] = white2.func(input1 , input2);
                break;

            case 3 :
                White3 white3 = new White3();
                output[0] = white3.func(input1 , input2);
                break;

            case 4 :
                White4 white4 = new White4();
                output[0] = white4.func(input1 , input2);
                break;

            case 5 :
                White5 white5 = new White5();
                output[0] = white5.func(input1 , input2);
                break;
        }
        return output;
    }
}
