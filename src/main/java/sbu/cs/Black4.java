package sbu.cs;

public class Black4 implements BlackFunction{

    @Override
    public String func(String arg) {
        StringBuilder sb = new StringBuilder(arg);
        char temp = sb.charAt(sb.length()-1);
        sb.delete(sb.length()-1,sb.length());
        sb.insert(0,temp);
        return sb.toString();
    }
}
