package sbu.cs;

public class White2 implements WhiteFunction{

    @Override
    public String func(String arg1, String arg2) {
        StringBuilder sbReverse = new StringBuilder(arg2);
        StringBuilder sbAnswer = new StringBuilder(arg1);
        sbAnswer.append(sbReverse.reverse());
        return sbAnswer.toString();
    }
}
