package sbu.cs;

public class White3 implements WhiteFunction{

    @Override
    public String func(String arg1, String arg2) {
        StringBuilder inputReverse = new StringBuilder(arg2);
        String input2 = inputReverse.reverse().toString();
        StringBuilder sb = new StringBuilder("");
        if (arg1.length() > input2.length()){
            sb.append(arg1);
            int j = 1 ;
            for (int i = 0 ; i < input2.length() ; i ++){
                sb.insert( j , input2.charAt(i));
                j +=2 ;
            }
        }
        else{
            sb.append(input2);
            int j = 0 ;
            for (int i = 0 ; i < arg1.length() ; i ++){
                sb.insert( j , arg1.charAt(i));
                j +=2 ;
            }
        }
        return sb.toString();
    }
}
