package sbu.cs;

public class Black5 implements BlackFunction {

    @Override
    public String func(String arg) {
        StringBuilder sb = new StringBuilder("");
        for (int i = 0 ; i < arg.length() ; i ++){
            int num = arg.charAt(i) - 97;
            char temp = (char) (122 - num);
            sb.append(temp);
        }
        return sb.toString();
    }
}
