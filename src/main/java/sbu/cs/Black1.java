package sbu.cs;

class Black1 implements BlackFunction{

    @Override
    public String func(String arg) {
        StringBuilder sb = new StringBuilder(arg);
        return sb.reverse().toString();
    }


}
